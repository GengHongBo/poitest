package easyExcel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.util.ListUtils;
import org.junit.Test;

import java.util.Date;
import java.util.List;

/**
 * 测试easyExcel使用
 * 官方指南：https://github.com/alibaba/easyexcel/blob/master/quickstart.md
 * easyExcel项目地址：https://github.com/alibaba/easyexcel
 */
public class EasyExcelWriteTest {

    /**
     * 最简单的写
     * <p>1. 创建excel对应的实体对象 参照{@link com.alibaba.easyexcel.test.demo.write.DemoData}
     * <p>2. 直接写即可
     */
    @Test
    public void simpleWrite() {
        String fileName = "D:\\系统默认\\桌面\\POITest\\src\\main\\java\\easyExcel写入表格.xlsx";
        // 这里 需要指定写用哪个class去读，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        // 如果这里想使用03 则 传入excelType参数即可
        long begin = System.currentTimeMillis();
        EasyExcel.write(fileName, DemoData.class).sheet("模板").doWrite(data());
        long end = System.currentTimeMillis();
        System.out.println((end-begin)/1000+"秒");
    }

    private List<DemoData> data() {
        List<DemoData> list = ListUtils.newArrayList();
        for (int i = 0; i < 1000000; i++) {
            DemoData data = new DemoData();
            data.setString("字符串" + i);
            data.setDate(new Date());
            data.setDoubleData(0.56);
            list.add(data);
        }
        return list;
    }


}
