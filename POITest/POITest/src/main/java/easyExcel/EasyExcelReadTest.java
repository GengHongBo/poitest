package easyExcel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.util.ListUtils;
import org.junit.Test;

import java.util.Date;
import java.util.List;

/**
 * 测试easyExcel使用
 * 官方指南：https://github.com/alibaba/easyexcel/blob/master/quickstart.md
 * easyExcel项目地址：https://github.com/alibaba/easyexcel
 */
public class EasyExcelReadTest {

    /**
     * 最简单的读
     * <p>1. 创建excel对应的实体对象 参照{@link DemoData}
     * <p>2. 由于默认一行行的读取excel，所以需要创建excel一行一行的回调监听器，参照{@link DemoDataListener}
     * <p>3. 直接读即可
     */
    @Test
    public void simpleRead() {
        String fileName = "D:\\系统默认\\桌面\\POITest\\src\\main\\java\\easyExcel写入表格.xlsx";
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
        //只能读取xlsx文件,读取100万数据需要17秒
        long begin = System.currentTimeMillis();
        EasyExcel.read(fileName, DemoData.class, new DemoDataListener()).sheet().doRead();
        long end = System.currentTimeMillis();
        System.out.println((end-begin)/1000+"秒");
    }




}
