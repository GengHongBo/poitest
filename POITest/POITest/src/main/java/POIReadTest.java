//import org.apache.poi.hssf.usermodel.*;
//import org.apache.poi.ss.usermodel.*;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.joda.time.DateTime;
//import org.junit.Test;
//
//import java.io.FileInputStream;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.Date;
//
//public class POIReadTest {
//
//    //03版本读取测试
//    @Test
//    public void read03Test() throws IOException {
//        long begin = System.currentTimeMillis();
//        FileInputStream fileInputStream = new FileInputStream("" +
//                "D:\\系统默认\\桌面\\POITest\\src\\main\\java\\03版表格.xls");
//        //读取一个工作薄
//        //HSSFWorkbook-03版 XSSFWorkbook-07版 SXSSFWorkbook-07版加速使用
//        Workbook workbook = new HSSFWorkbook(fileInputStream);
//        //读第一个工作表sheet
//        Sheet sheetAt = workbook.getSheetAt(0);
//        //读取第一行
////        Row row = sheetAt.getRow(0);
//        Row row = sheetAt.getRow(1);
//        //读取第一行第一列
////        Cell cell = row.getCell(0);
//        Cell cell = row.getCell(1);
//        System.out.println(cell.getStringCellValue());//读取时可能遇到读取类型错误，需要注意
//
//        fileInputStream.close();
//
//        long end = System.currentTimeMillis();
//        System.out.println((end-begin)/1000+"秒");
//    }
//
//    //07版本读取测试
//    @Test
//    public void read07Test() throws IOException {
//        long begin = System.currentTimeMillis();
//        FileInputStream fileInputStream = new FileInputStream("" +
//                "D:\\系统默认\\桌面\\POITest\\src\\main\\java\\07版表格.xlsx");
//        //读取一个工作薄
//        //HSSFWorkbook-03版 XSSFWorkbook-07版 SXSSFWorkbook-07版加速使用
//        Workbook workbook = new XSSFWorkbook(fileInputStream);
//        //读第一个工作表sheet
//        Sheet sheetAt = workbook.getSheetAt(0);
//        //读取第一行
////        Row row = sheetAt.getRow(0);
//        Row row = sheetAt.getRow(1);
//        //读取第一行第一列
////        Cell cell = row.getCell(0);
//        Cell cell = row.getCell(1);
//        System.out.println(cell.getStringCellValue());//读取时可能遇到读取类型错误，需要注意
//
//        fileInputStream.close();
//
//        long end = System.currentTimeMillis();
//        System.out.println((end-begin)/1000+"秒");
//    }
//
//    //03版本读取不同类型数据测试
//    @Test
//    public void read03DifferentTypeTest() throws IOException {
//        long begin = System.currentTimeMillis();
//        FileInputStream fileInputStream = new FileInputStream("" +
//                "D:\\系统默认\\桌面\\POITest\\src\\main\\java\\明细表.xls");
//        //读取一个工作薄
//        //HSSFWorkbook-03版 XSSFWorkbook-07版 SXSSFWorkbook-07版加速使用
//        Workbook workbook = new HSSFWorkbook(fileInputStream);
//
//        //读取表头
//        Sheet sheetAt = workbook.getSheetAt(0);
//        Row row = sheetAt.getRow(0);
//        int cellCount = row.getPhysicalNumberOfCells();//获取一行的列长度
//        for(int cellNum = 0; cellNum < cellCount; cellNum ++){
//            Cell cell = row.getCell(cellNum);
//            if(null != cell){//判断是否为空
//                System.out.print(cell.getStringCellValue()+"|");
//            }
//        }
//
//        System.out.println();
//        //读取除表头以外的内容
//        int cellRow = sheetAt.getPhysicalNumberOfRows();//读取
//        for(int i = 1;i < cellRow;i++){
//            Row rowData = sheetAt.getRow(i);
//            for(int j=0;j<cellCount;j++){
//                System.out.print("[第"+i+"-"+j+"列]");
//                Cell cell = rowData.getCell(j);
//                if(null !=cell){
//                    int cellType = cell.getCellType();//获取表格类型
//                    String cellValue = "";
//                    switch (cellType){
//                        case HSSFCell.CELL_TYPE_STRING://字符串类型
//                            System.out.print("【STRING】");
//                            cellValue = cell.getStringCellValue();
//                            break;
//                        case HSSFCell.CELL_TYPE_BOOLEAN:
//                            System.out.print("【BOOLEAN】");
//                            cellValue = String.valueOf(cell.getBooleanCellValue());
//                            break;
//                        case HSSFCell.CELL_TYPE_BLANK:
//                            System.out.print("【BLANK】");
//                            break;
//                        case HSSFCell.CELL_TYPE_NUMERIC://（数字（日期，普通数字））
//                            System.out.print("【NUMERIC】");
//                            if(HSSFDateUtil.isCellDateFormatted(cell)){
//                                System.out.print("【日期】");
//                                Date dateCellValue = cell.getDateCellValue();
//                                cellValue = new DateTime(dateCellValue).toString("yyyy-MM-dd");
//                            }else{
//                                System.out.print("数字");
//                                cell.setCellType(HSSFCell.CELL_TYPE_STRING);
//                                cellValue = cell.toString();
//                            }
//                            break;
//                        case HSSFCell.CELL_TYPE_ERROR:
//                            System.out.print("数据类型错误");
//                            break;
//                    }
//                    System.out.println(cellValue);
//                }
//            }
//
//        }
//
//        fileInputStream.close();
//
//        long end = System.currentTimeMillis();
//        System.out.println((end-begin)/1000+"秒");
//    }
//
//    //03版本计算公式获取(实际工作中使用较少，仅作为参考使用)
//    @Test
//    public void read03formulaTest() throws IOException {
//        long begin = System.currentTimeMillis();
//        FileInputStream fileInputStream = new FileInputStream("" +
//                "D:\\系统默认\\桌面\\POITest\\src\\main\\java\\公式.xls");
//        //读取一个工作薄
//        //HSSFWorkbook-03版 XSSFWorkbook-07版 SXSSFWorkbook-07版加速使用
//        Workbook workbook = new HSSFWorkbook(fileInputStream);
//
//        //获取计算公式
//        FormulaEvaluator hssfFormulaEvaluator = new HSSFFormulaEvaluator((HSSFWorkbook) workbook);
//        Sheet sheetAt = workbook.getSheetAt(0);
//        Row row = sheetAt.getRow(3);
//        Cell cell = row.getCell(0);
//        if(null != cell){
//            int cellType = cell.getCellType();
//            switch (cellType){
//                case Cell.CELL_TYPE_FORMULA:
//                    String cellFormula = cell.getCellFormula();//输出公式
//                    System.out.println(cellFormula);
//
//                    CellValue evaluate = hssfFormulaEvaluator.evaluate(cell);//输出公式计算后的值
//                    System.out.println(evaluate.formatAsString());
//            }
//
//        }
//
//
//
//        fileInputStream.close();
//        long end = System.currentTimeMillis();
//        System.out.println((end-begin)/1000+"秒");
//    }
//}
