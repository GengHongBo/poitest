import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;

public class POIWriteTest {

    //03版本写入测试
    @Test
    public void writeTest() throws IOException {
        long begin = System.currentTimeMillis();
        //创建一个工作薄
        //HSSFWorkbook-03版 XSSFWorkbook-07版 SXSSFWorkbook-07版加速使用
        Workbook workbook = new HSSFWorkbook();

        //创建一个工作表
        Sheet sheet = workbook.createSheet("工作表1");

        //第一行 创建两个单元格
        Row row11 = sheet.createRow(0);//第一行
        Cell cell11 = row11.createCell(0);//第一行第一个单元格
        //设置元素
        cell11.setCellValue("内容");
        Cell cell12 = row11.createCell(1);//第一行第二个单元格
        //设置元素
        cell12.setCellValue("234234");

        //创建两个单元格
        Row row21 = sheet.createRow(1);//第二行
        Cell cell21 = row21.createCell(0);//第二行第一个单元格
        //设置元素
        cell21.setCellValue("时间");
        Cell cell22 = row21.createCell(1);//第二行第而个单元格
        //设置元素
        String time = new DateTime().toString("yyyy-MM-dd HH:mm:ss");
        cell22.setCellValue(time);

        FileOutputStream fileOutputStream = new FileOutputStream(
                "D:\\系统默认\\桌面\\POITest\\src\\main\\java\\03版表格.xls");
        workbook.write(fileOutputStream);
        fileOutputStream.close();

        long end = System.currentTimeMillis();
        System.out.println((end-begin)/1000+"秒");
    }

    //07版本测试
    @Test
    public void write07Test() throws IOException {
        long begin = System.currentTimeMillis();
        //创建一个工作薄
        //HSSFWorkbook-03版 XSSFWorkbook-07版 SXSSFWorkbook-07版加速使用
        Workbook workbook = new XSSFWorkbook();

        //创建一个工作表
        Sheet sheet = workbook.createSheet("工作表1");

        //第一行 创建两个单元格
        Row row11 = sheet.createRow(0);//第一行
        Cell cell11 = row11.createCell(0);//第一行第一个单元格
        //设置元素
        cell11.setCellValue("内容");
        Cell cell12 = row11.createCell(1);//第一行第二个单元格
        //设置元素
        cell12.setCellValue("234234");

        //创建两个单元格
        Row row21 = sheet.createRow(1);//第二行
        Cell cell21 = row21.createCell(0);//第二行第一个单元格
        //设置元素
        cell21.setCellValue("时间");
        Cell cell22 = row21.createCell(1);//第二行第而个单元格
        //设置元素
        String time = new DateTime().toString("yyyy-MM-dd HH:mm:ss");
        cell22.setCellValue(time);

        FileOutputStream fileOutputStream = new FileOutputStream(
                "D:\\系统默认\\桌面\\POITest\\src\\main\\java\\07版表格.xlsx");
        workbook.write(fileOutputStream);
        fileOutputStream.close();
        long end = System.currentTimeMillis();
        System.out.println((end-begin)/1000+"秒");
    }

    //03版本大数据量写入测试
    //1. 最大65536行内容，超过报异常，写入时间1秒
    @Test
    public void write03BigDataTest() throws IOException {
        long begin = System.currentTimeMillis();
        //创建一个工作薄
        //HSSFWorkbook-03版 XSSFWorkbook-07版 SXSSFWorkbook-07版加速使用
        Workbook workbook = new HSSFWorkbook();

        //创建一个工作表
        Sheet sheet = workbook.createSheet("工作表1");

        for(int i=0;i <65536;i++){
            Row row = sheet.createRow(i);//行
            for(int j=0;j<10;j++){
                //填充10列的单元格内容
                Cell cell = row.createCell(j);
                cell.setCellValue(j);
            }
        }

        FileOutputStream fileOutputStream = new FileOutputStream(
                "D:\\系统默认\\桌面\\POITest\\src\\main\\java\\03版大数据表格.xls");
        workbook.write(fileOutputStream);
        fileOutputStream.close();

        long end = System.currentTimeMillis();
        System.out.println((end-begin)/1000+"秒");
    }

    //07版本大数据量写入测试
    //可以插入无限量数据，需要优化
    //时间过慢，停止，报错，堆内存溢出java.lang.OutOfMemoryError: Java heap space
    @Test
    public void write07BigDataTest() throws IOException {
        long begin = System.currentTimeMillis();
        //创建一个工作薄
        //HSSFWorkbook-03版 XSSFWorkbook-07版 SXSSFWorkbook-07版加速使用
        Workbook workbook = new XSSFWorkbook();

        //创建一个工作表
        Sheet sheet = workbook.createSheet("工作表1");

        for(int i=0;i <1000000;i++){
            Row row = sheet.createRow(i);//行
            for(int j=0;j<10;j++){
                //填充10列的单元格内容
                Cell cell = row.createCell(j);
                cell.setCellValue(j);
            }
        }

        FileOutputStream fileOutputStream = new FileOutputStream(
                "D:\\系统默认\\桌面\\POITest\\src\\main\\java\\07版大数据表格.xls");
        workbook.write(fileOutputStream);
        fileOutputStream.close();

        long end = System.currentTimeMillis();
        System.out.println((end-begin)/1000+"秒");
    }

    //07版本大数据量写入测试-优化版本
    //可以插入无限量数据，时间大大优化
    //100万需要8秒插入
    @Test
    public void write07BetterBigDataTest() throws IOException {
        long begin = System.currentTimeMillis();
        //创建一个工作薄
        //HSSFWorkbook-03版 XSSFWorkbook-07版 SXSSFWorkbook-07版加速使用
        Workbook workbook = new SXSSFWorkbook();

        //创建一个工作表
        Sheet sheet = workbook.createSheet("工作表1");

        for(int i=0;i <1000000;i++){
            Row row = sheet.createRow(i);//行
            for(int j=0;j<10;j++){
                //填充10列的单元格内容
                Cell cell = row.createCell(j);
                cell.setCellValue(j);
            }
        }

        FileOutputStream fileOutputStream = new FileOutputStream(
                "D:\\系统默认\\桌面\\POITest\\src\\main\\java\\07版优化大数据表格.xlsx");
        workbook.write(fileOutputStream);
        //关闭临时文件
        ((SXSSFWorkbook)workbook).dispose();
        fileOutputStream.close();

        long end = System.currentTimeMillis();
        System.out.println((end-begin)/1000+"秒");
    }
}
